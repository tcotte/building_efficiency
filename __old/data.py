import pandas as pd
from Utils.constants import *
from copy import deepcopy
from numpy import random


class DataReader(object):
    """
    Singleton class to read CSV file once for all.
    """
    instance = None

    def __new__(cls):
        if DataReader.instance is None:
            DataReader.instance = pd.read_csv(FILE_PATH, names=BASE_COLUMNS, sep=',', header=0)
        return DataReader.instance

    def __getattr__(self, attr):
        return getattr(self.instance, attr)

    def __setattr__(self, attr, val):
        return setattr(self.instance, attr, val)


class Data(pd.DataFrame):
    @staticmethod
    def get_data():
        d = deepcopy(DataReader())
        d.__class__ = Data
        return d

    def normalize(self, columns):
        return self
    """
    pandas.DataFrame extension with specific methods



    # tests :
    def rename_column(self, old_name, new_name):
        self.rename(columns={old_name: new_name}, inplace=True)
        return self

    def say_hello(self):
        print("Hello world !")
        return self

    # TODO :
    # 1 Generic methods
    # 1.1 Splitting
    def split(self, percent=80):
        rep = random.rand(len(self)) < percent/100
        return self[rep], self[~rep]

    # 1.2 Encoding
    def one_hot_encode(self, columns_list):
        return self

    def label_encode(self, columns_list):
        return self

    # 2 Specific methods
    # 2.1 Columns constructions
    def get_total_load(self):
        return self[Y1] + self[Y2]

    def set_total_load_column(self):
        self["Total load"] = self.get_total_load()
        return self

    def get_energy_class(self):
        return [
            "A" if tl < 30 else "B" if tl < 35 else "C" if tl < 45 else "D" if tl < 55 else "E" if tl < 65 else "F" if tl < 75 else "G"
            for tl in self.get_total_load()]

    def set_energy_class_column(self):
        self["Energy class"] = self.get_energy_class()
        return self

    def drop_load_columns(self):
        self.drop([Y1, Y2], axis=1, inplace=True)
        return self

    def extend_columns(self):
        self.set_total_load_column()
        self.set_energy_class_column()
        return self

    def to_classification(self):
        self.set_energy_class_column()
        self.drop_load_columns()
        return self

    def to_regression(self):
        self.set_total_load_column()
        self.drop_load_columns()
        return self
    """