from sklearn.base import BaseEstimator, TransformerMixin


class SumEfficiency(BaseEstimator, TransformerMixin):
    def __init__(self, features_names):
        self._features_names = features_names

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        X['Label'] = X[self._features_names[0]] + X[self._features_names[1]]
        return X


class LabelEfficiency(BaseEstimator, TransformerMixin):
    def __init__(self, feature):
        self._feature = feature

    def fit(self, X, y=None):
        return self

    def set_letter(self, X):
        if X[self._feature] < 30:
            return 'A'
        elif X[self._feature] < 35:
            return 'B'
        elif X[self._feature] < 45:
            return 'C'
        elif X[self._feature] < 55:
            return 'D'
        elif X[self._feature] < 65:
            return 'E'
        elif X[self._feature] < 75:
            return 'F'
        else:
            return 'G'

    def transform(self, X, y=None):
        X['Efficiency'] = X.apply(self.set_letter, axis=1)
        return X


class FeatureDropper(BaseEstimator, TransformerMixin):
    def __init__(self, feature_names):
        self._feature_names = feature_names

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        X = X.drop(columns = self._feature_names)
        return X