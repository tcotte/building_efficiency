from sklearn.model_selection import cross_val_predict
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import numpy as np

def compute_conf_matrix(estimator, X, y, cv=3): 
    y_train_pred = estimator.predict(X)
    return confusion_matrix(y, y_train_pred)


def display_conf_matrix(conf_mx, color=True):
    if color:
        plt.figure(figsize=(5, 5))
        plt.matshow(conf_mx, cmap=plt.cm.gray)
        plt.xlabel('Prédictions')
        plt.ylabel('Labels réels')
        plt.title('Matrice de confusion \n')
        plt.xticks(range(7), ['A', 'B', 'C', 'D', 'E', 'F', 'G'])
        plt.yticks(range(7), ['A', 'B', 'C', 'D', 'E', 'F', 'G'])
        plt.show()
        
    else:
        print(conf_mx)
        
        
def plot_error_analysis(conf_mx):
    row_sums = conf_mx.sum(axis=1, keepdims=True)
    norm_conf_mx = conf_mx / row_sums
    
    np.fill_diagonal(norm_conf_mx, 0)
    plt.figure(figsize=(5, 5))
    plt.matshow(norm_conf_mx, cmap=plt.cm.gray)
    plt.xlabel('Prédictions')
    plt.ylabel('Labels réels')
    plt.xticks(range(7), ['A', 'B', 'C', 'D', 'E', 'F', 'G'])
    plt.yticks(range(7), ['A', 'B', 'C', 'D', 'E', 'F', 'G'])
    plt.title("Analyse des erreurs uniquement \n")
    
    plt.show()