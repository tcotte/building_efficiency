def set_letters_pred(y):
    y_pred_label = []
    for val in y:
        if val < 30:
            y_pred_label.append('A')
        elif val < 35:
            y_pred_label.append('B')
        elif val < 45:
            y_pred_label.append('C')
        elif val < 55:
            y_pred_label.append('D')
        elif val < 65:
            y_pred_label.append('E')
        elif val < 75:
            y_pred_label.append('F')
        else:
            y_pred_label.append('G')
    return y_pred_label


