from sklearn.base import BaseEstimator, TransformerMixin


# Transform Data to float
class FloatData(BaseEstimator, TransformerMixin):
    def __init__(self):
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return X.astype(float)