PATH = "Data/"
FILENAME = "DataEnergy.csv"
FILEPATH = PATH + FILENAME

X1='Relative compactness'
X2='Surface area'
X3='Wall area'
X4='Roof area'
X5='Overall height'
X6='Orientation'
X7='Glazing area'
X8='Glazing area distr'
Y1='Energy'
Y2='Energy efficiency'

BASE_COLUMNS = [X1,X2,X3,X4,X5,X6,X7,X8,Y1,Y2]

FEATURES = [X1,X2,X3,X4,X5,X6,X7,X8]
LABELS=[Y1,Y2]


CATEGORICAL = [X6, X8]
NUMERICAL = [col for col in FEATURES if col not in CATEGORICAL]
