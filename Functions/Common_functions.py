import pandas as pd
import os
from Functions.constants import *

from sklearn.pipeline import Pipeline
from Functions.transformer import FloatData
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split


def load_data(path="Data/", filename="DataEnergy.csv", columns=BASE_COLUMNS):
    df = pd.read_csv(os.path.join(path, filename),
                     names=columns,
                     sep=',',
                     header=0)
    return df


def formate_dataset(path="Data/", normalizer=True):
    eng_df = load_data(path, "DataEnergy.csv", BASE_COLUMNS)
    preprocessor = create_prepocessor(NUMERICAL, CATEGORICAL, normalize=normalizer)
    dataset = preprocessor.fit_transform(eng_df)
    return create_df(dataset)


def create_prepocessor(numeric_features, categorical_features, normalize):
    numeric_transformer = create_num_tf(normalize)

    categorical_transformer = Pipeline(steps=[
        ('onehot', OneHotEncoder(handle_unknown='ignore'))
    ])
    preprocessor = ColumnTransformer(
        transformers=[
            ('numeric', numeric_transformer, numeric_features),
            ('category', categorical_transformer, categorical_features),
        ], remainder='passthrough')

    return preprocessor


def create_num_tf(normalize):
    if normalize:
        numeric_transformer = Pipeline(steps=[
            ('to_float', FloatData()),
            ('normalize_X', StandardScaler())
        ])
    else:
        numeric_transformer = Pipeline(steps=[
            ('to_float', FloatData()),
        ])
    return numeric_transformer


def create_df(dataset):
    new_columns = NUMERICAL + ['GAD : 0', 'GAD : 1', 'GAD : 2', 'GAD : 3', 'GAD : 4', 'GAD : 5', 'East', 'North', 'South', 'West'] + LABELS
    dataset_df = pd.DataFrame(data=dataset, columns=new_columns)
    return dataset_df

def class_split(dataset_df, pourcent):
    return train_test_split(dataset_df[dataset_df.columns[0:16]],
                                                            dataset_df['Energy efficiency'], test_size=pourcent,
                                                            random_state=42)

def reg_split(dataset_df, pourcent):
    return train_test_split(dataset_df[dataset_df.columns[0:16]], dataset_df['Energy'],
                                                 test_size=pourcent, random_state=42)

# Compare values between y_test_b and y_pred
def score(y_pred_label, y_test_b):
    true_positive = 0
    for ind, letter in enumerate(y_pred_label):
        if y_pred_label[ind] == y_test_b.array[ind]:
            true_positive += 1
    return (true_positive / len(y_pred_label)) * 100